package com.mike.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.mike")
//@ComponentScan(basePackages = "com.mike.infrastructure.soap")
public class App {
    public static void main(String[] args){
        SpringApplication.run(App.class, args);
    }
}
