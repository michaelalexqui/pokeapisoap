package com.mike.domain.repository;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.model.Pokemon;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface PokemonPersistencePort {
    PagedPokemonList getPokemonList(Integer limit, Integer offSet);
}
