package com.mike.domain.spi;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.model.Pokemon;
import org.springframework.stereotype.Service;

import java.util.List;
public interface GetPokemonListServicePort {
    PagedPokemonList execute(Integer limit, Integer offSet);
}
