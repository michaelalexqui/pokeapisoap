package com.mike.domain.usecase;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.model.Pokemon;
import com.mike.domain.repository.PokemonPersistencePort;
import com.mike.domain.spi.GetPokemonListServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@RequiredArgsConstructor
@Service
public class GetPokemonListUseCase implements GetPokemonListServicePort {
    private final PokemonPersistencePort pokemonPersistencePort;
    @Override
    public PagedPokemonList execute(Integer limit, Integer offSet) {
        return pokemonPersistencePort.getPokemonList(limit, offSet);
    }
}
