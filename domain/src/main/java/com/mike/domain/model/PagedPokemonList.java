package com.mike.domain.model;

import lombok.*;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PagedPokemonList {
    private List<Pokemon> results;
    private Integer count;
    private String next;
    private String previous;
}
