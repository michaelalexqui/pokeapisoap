package com.mike.infrastructure.soap.dto;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.*;

@XmlRootElement(name = "sayHello"
        , namespace = "http://mike.com/services"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestHelloDto {
    private String name;

    //@XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
