package com.mike.infrastructure.soap.dto;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "getPokemonList"
        , namespace = "http://mike.com/services"
)
public class GetPokemonListRequestDto {
    private Integer limit;
    private Integer offSet;

    @XmlElement(name = "limit")
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
    @XmlElement(name = "offset")
    public Integer getOffSet() {
        return offSet;
    }

    public void setOffSet(Integer offSet) {
        this.offSet = offSet;
    }
}
