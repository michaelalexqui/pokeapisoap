package com.mike.infrastructure.soap.dto;

import jakarta.xml.bind.annotation.XmlElement;
import lombok.ToString;

@ToString
public class PokemonResponseDto {
    private String name;
    private String url;

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
