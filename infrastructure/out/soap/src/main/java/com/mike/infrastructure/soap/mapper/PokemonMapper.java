package com.mike.infrastructure.soap.mapper;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.model.Pokemon;
import com.mike.infrastructure.soap.dto.GetPokemonListResponseDto;
import com.mike.infrastructure.soap.dto.PokemonResponseDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PokemonMapper {
    default GetPokemonListResponseDto domainListToDtoList(PagedPokemonList pokemonResponseDto){
        return GetPokemonListResponseDto.builder()
                .result(modelToDto(pokemonResponseDto.getResults()))
                .next(pokemonResponseDto.getNext())
                .count(pokemonResponseDto.getCount())
                .previous(pokemonResponseDto.getPrevious()).build();
    }
    List<PokemonResponseDto> modelToDto(List<Pokemon> pokemon);
}
