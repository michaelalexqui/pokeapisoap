package com.mike.infrastructure.soap.api;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.spi.GetPokemonListServicePort;
import com.mike.infrastructure.soap.dto.*;
import com.mike.infrastructure.soap.mapper.PokemonMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class GetPokemonListController {
    private static final String NAMESPACE_URI = "http://mike.com/services";
    private final GetPokemonListServicePort getPokemonListServicePort;
    private final PokemonMapper pokemonMapper;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPokemonList")
    @ResponsePayload
    public GetPokemonListResponseDto getPokemonList(@RequestPayload GetPokemonListRequestDto request){
        GetPokemonListResponseDto response = new GetPokemonListResponseDto();
        PagedPokemonList rawResponse = getPokemonListServicePort.execute(request.getLimit(), request.getOffSet());
        return pokemonMapper.domainListToDtoList(rawResponse);
    }
}
