package com.mike.infrastructure.soap.dto;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@XmlRootElement(name = "getPokemonList"
        , namespace = "http://mike.com/services"
)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetPokemonListResponseDto {

    List<PokemonResponseDto> result;
    private Integer count;
    private String next;
    private String previous;

    @XmlElement(name = "data")
    public List<PokemonResponseDto> getResult() {
        return result;
    }

    public void setResult(List<PokemonResponseDto> result) {
        this.result = result;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    @XmlElement(name = "count")
    public Integer getCount() {
        return count;
    }
    @XmlElement(name = "next")
    public String getNext() {
        return next;
    }
    @XmlElement(name = "previous")
    public String getPrevious() {
        return previous;
    }
}
