package com.mike.infrastructure.restclient.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PokemonInDto {
    private String name;
    private String url;
}
