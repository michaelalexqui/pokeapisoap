package com.mike.infrastructure.restclient.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PokemonListInDto {
    private List<PokemonInDto> results;
    private Integer count;
    private String next;
    private String previous;
}
