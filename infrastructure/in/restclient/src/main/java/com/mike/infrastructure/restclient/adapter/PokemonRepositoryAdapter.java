package com.mike.infrastructure.restclient.adapter;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.model.Pokemon;
import com.mike.domain.repository.PokemonPersistencePort;
import com.mike.infrastructure.restclient.dto.PokemonListInDto;
import com.mike.infrastructure.restclient.dto.PokemonResponseListDto;
import com.mike.infrastructure.restclient.mapper.PokemonRestMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
@RequiredArgsConstructor
public class PokemonRepositoryAdapter implements PokemonPersistencePort {
    private final PokemonRestMapper pokemonRestMapper;
    @Override
    public PagedPokemonList getPokemonList(Integer limit, Integer offSet) {
        System.out.println("limit:"+limit+" offset: "+offSet );
        Map<String, Integer> params = new HashMap<>();
        params.put("limit", limit);
        params.put("offset", offSet);
        WebClient client = WebClient.builder()
                .baseUrl("https://pokeapi.co")
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
        Mono<PokemonListInDto> restResponse = client.get().uri("/api/v2/pokemon?limit={limit}&offset={offset}", params).retrieve().bodyToMono(PokemonListInDto.class);
        PagedPokemonList response = pokemonRestMapper.responseListToModelList(restResponse.block());
        System.out.println("Response: "+response.toString());
        return response;
    }
}
