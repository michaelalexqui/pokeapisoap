package com.mike.infrastructure.restclient.dto;

import com.mike.domain.model.Pokemon;
import lombok.*;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PokemonResponseListDto {
    private List<Pokemon> results;
    private Integer count;
    private String next;
    private String previous;
}
