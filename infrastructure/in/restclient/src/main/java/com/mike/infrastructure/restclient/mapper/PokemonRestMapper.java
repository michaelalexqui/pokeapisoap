package com.mike.infrastructure.restclient.mapper;

import com.mike.domain.model.PagedPokemonList;
import com.mike.domain.model.Pokemon;
import com.mike.infrastructure.restclient.dto.PokemonInDto;
import com.mike.infrastructure.restclient.dto.PokemonListInDto;
import com.mike.infrastructure.restclient.dto.PokemonResponseListDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PokemonRestMapper {
    PagedPokemonList responseListToModelList(PokemonListInDto pokemonListInDto);
    Pokemon dtoToModel(PokemonInDto pokemonInDto);
}
